import React from "react";
import Input from "../component/InputPage";
import Navbar from "../component/NavbarInput";

export default function InputPage() {
  return (
    <>
      <Navbar />
      <Input />
    </>
  );
}
