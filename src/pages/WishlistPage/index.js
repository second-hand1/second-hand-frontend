import React from "react";
import { Link } from "react-router-dom";
import CurrencyFormatter from "../../modules/CurrencyFormatter";
import NavbarHome from "../NavbarHome/NavbarHome";
import "./styles.css";
import EmptyList from "../../components/EmptyList";

const WishlistPage = () => {
  const wishlist = JSON.parse(localStorage.getItem("wishlist"));

  return (
    <div className="container-fluid">
      <NavbarHome />
      <div className="row justify-content-center wishlist-page" style={{ marginTop: "100px" }}>
        <div className="col-lg-9">
          <h3 className="title">Daftar Wishlistmu</h3>
          <div>
            {wishlist == undefined ? (
              <EmptyList type="wishlist" />
            ) : wishlist.length === 0 ? (
              <EmptyList type="wishlist" />
            ) : (
              wishlist.map((item) => {
                const image = JSON.parse(item.data.foto_produk);
                return (
                  <div
                    key={item.id}
                    className="product-card d-flex justify-content-between align-items-center ">
                    <Link
                      to={`/detailPenawaran?id=${item.id}`}
                      className="d-flex w-100 justify-content-between align-items-center left ">
                      <img src={image[0]} alt={item.data.nama_produk} />
                      <div className="flex-fill ms-3">
                        <p className="title">{item.data.nama_produk}</p>
                        <p className="kategori">{item.data.kategori}</p>
                        <p className="price">Rp {CurrencyFormatter(item.data.harga_produk)}</p>
                      </div>
                    </Link>
                    <button
                      onClick={() => {
                        const wishlist = JSON.parse(localStorage.getItem("wishlist"));
                        const newWishlist = [...wishlist];

                        newWishlist.splice(
                          newWishlist.findIndex((item) => item.id === item.id),
                          1
                        );

                        localStorage.setItem("wishlist", JSON.stringify(newWishlist));
                        window.location.reload();
                      }}
                      className="btn d-flex justify-content-between align-items-center ">
                      <img src="./icons/delete.svg" alt="delete" width={20} height={20} />
                      Hapus
                    </button>
                  </div>
                );
              })
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default WishlistPage;
