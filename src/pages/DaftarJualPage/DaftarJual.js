import React from "react";
import NavbarHome from "../NavbarHome/NavbarHome";
import DaftarJualProduct from "./index";

export default function DaftarJual() {
  return (
    <>
      <NavbarHome />
      <DaftarJualProduct />
    </>
  );
}
