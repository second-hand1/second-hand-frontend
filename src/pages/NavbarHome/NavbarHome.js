import React, { useState } from "react";
import "./NavbarHome.css";
import {
  Container,
  Navbar,
  Nav,
  FormControl,
  InputGroup,
  Button,
  Offcanvas,
  Row,
  Col
} from "react-bootstrap";
import { BiSearch } from "react-icons/bi";
import { GiHamburgerMenu } from "react-icons/gi";
import fi_list from "../../assets/images/fi_list.png";
import fi_bell from "../../assets/images/fi_bell.png";
import fi_user from "../../assets/images/fi_user.png";
import btnMasuk from "../../assets/images/btnMasuk.png";

export default function NavbarHome() {
  const bearerToken = localStorage.getItem("token");
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const isLogin = bearerToken !== null;

  return (
    <div className="fixed-top">
      <Navbar bg="light shadow-sm" expand="lg" id="NavDekstop">
        <Container>
          <Navbar.Brand href="/" className="navbarLogo"></Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <InputGroup className="d-flex wrapper">
              <FormControl
                className="customSearch"
                type="search"
                placeholder="Cari disini..."
                aria-label="search"
                aria-describedby="basic-addon2"
              />
              <InputGroup.Text id="basic-addon2" className="customSearchIcon">
                <BiSearch />
              </InputGroup.Text>
            </InputGroup>
            <Nav className="ms-auto my-2 my-lg-0">
              {isLogin ? (
                <>
                  <Nav.Link href="/daftarJual">
                    <img src={fi_list} alt="list" />
                  </Nav.Link>
                  <Nav.Link href="/notifikasi">
                    <img src={fi_bell} alt="bell" />
                  </Nav.Link>
                  <Nav.Link href="/profile">
                    <img src={fi_user} alt="user" />
                  </Nav.Link>
                </>
              ) : (
                <>
                  <Nav.Link href="/login">
                    <img src={btnMasuk} alt="masuk" />
                  </Nav.Link>
                </>
              )}
              <Nav.Link href="/wishlist">
                <img src="./icons/favorite.svg" alt="favorite" className="my-auto" />
              </Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <Container className="mt-4" id="SidebarMobile">
        <Row>
          <Col>
            <Button variant="light btnMobile" onClick={handleShow}>
              <GiHamburgerMenu />
            </Button>
            <div className="sidebar">
              <Offcanvas show={show} onHide={handleClose}>
                <Offcanvas.Header closeButton>
                  <Offcanvas.Title>
                    <b>Second Hand</b>
                  </Offcanvas.Title>
                </Offcanvas.Header>
                <Offcanvas.Body>
                  {isLogin ? (
                    <ul>
                      <li>
                        <a href="#">Notifikasi</a>
                      </li>
                      <li>
                        <a href="#">Daftar Jual</a>
                      </li>
                      <li>
                        <a href="#">Akun Saya</a>
                      </li>
                    </ul>
                  ) : (
                    <a href="#">
                      <img src={btnMasuk} alt="masuk" />
                    </a>
                  )}
                </Offcanvas.Body>
              </Offcanvas>
            </div>
          </Col>
          <Col>
            <InputGroup className="d-flex wrapper">
              <FormControl
                className="customSearch2"
                type="search"
                placeholder="Cari disini..."
                aria-label="search"
                aria-describedby="basic-addon2"
              />
              <InputGroup.Text id="basic-addon2" className="customSearchIcon2">
                <BiSearch />
              </InputGroup.Text>
            </InputGroup>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
