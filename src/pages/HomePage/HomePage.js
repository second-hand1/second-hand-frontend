import React from "react";
import Home from "../../components/Home/Home";
import Navbar from "../NavbarHome/NavbarHome";

export default function HomePage() {
  return (
    <>
      <Navbar />
      <Home />
    </>
  );
}
