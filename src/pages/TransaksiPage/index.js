import React, { useEffect, useState } from "react";
import "./styles.css";
import ProductCardLebar from "../../components/ProductCardLebar";
import axios from "axios";
import NavbarHome from "../NavbarHome/NavbarHome";
import jwt_decode from "jwt-decode";
import EmptyList from "../../components/EmptyList";
import DateNow from "../../modules/DateNow";

const TransaksiPage = () => {
  const bearerToken = localStorage.getItem("token");
  const isLogin = bearerToken !== null;
  const getUserData = jwt_decode(bearerToken);

  if (isLogin === false || getUserData.exp < DateNow()) {
    window.location.href = "/login";
  }

  const urlTransaksi = "https://secondhand-api-fsw14-kel3.herokuapp.com/api/transaksi-saya";

  const [transaksi, setTransaksi] = useState([]);

  const getTransaksi = async () => {
    await axios
      .get(urlTransaksi, {
        headers: {
          Authorization: `Bearer ${bearerToken}`
        }
      })
      .then((res) => {
        setTransaksi(res.data);
      });
  };

  useEffect(() => {
    getTransaksi();
  }, []);
  const dataTransaksi = transaksi && transaksi.data ? transaksi.data : [];

  return (
    <div className="container-fluid">
      <NavbarHome />
      <div className="row justify-content-center Transaksi-page" style={{ marginTop: "100px" }}>
        <div className="col-lg-9">
          <h3 className="title">Daftar Transaksimu</h3>
          <div>
            {dataTransaksi.length === 0 ? (
              <EmptyList type={"transaksi"} />
            ) : (
              dataTransaksi.map((item) => {
                const image = JSON.parse(item.Barang.foto_produk);
                return (
                  <ProductCardLebar
                    key={item.id_tawaran}
                    harga={item.Barang.harga_produk}
                    nama={item.Barang.nama_produk}
                    hargaPenawaran={item.nominal_bayar == null ? 0 : item.nominal_bayar}
                    tanggalPenawaran={item.updateAt}
                    type="Penawaran"
                    href="/transaksi"
                    image={image[0]}
                  />
                );
              })
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default TransaksiPage;
