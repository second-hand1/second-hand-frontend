import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import "./styles.css";
import NavbarHome from "../NavbarHome/NavbarHome";
import jwt_decode from "jwt-decode";
import DateNow from "../../modules/DateNow";

const ProfilePage = () => {
  const token = localStorage.getItem("token");
  const isLogin = token !== null;
  const getUserData = jwt_decode(token);

  if (isLogin === false || getUserData.exp < DateNow()) {
    window.location.href = "/login";
  }

  const [profileLocalStorage, setProfileLocalStorage] = useState([]);
  useEffect(() => {
    const getPost = async () => {
      await axios
        .get("https://secondhand-api-fsw14-kel3.herokuapp.com/api/users/profile", {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })
        .then((res) => {
          setProfileLocalStorage(res.data);
        });
    };
    getPost();
  }, []);
  const dataFotoUser = profileLocalStorage.data ? profileLocalStorage.data.foto_user : "loading";

  const logOut = () => {
    localStorage.removeItem("token");
    window.location.href = "/";
  };

  return (
    <div className="container-fluid">
      <NavbarHome />
      <div className="row justify-content-center input-profile" style={{ marginTop: "100px" }}>
        <div className="col-lg-6 px-auto">
          <div className="mx-auto">
            <label htmlFor="input-img" className="btn input-profile-img-btn mx-auto">
              <img src={dataFotoUser} alt="images" className="input-profile-img" />
            </label>
          </div>
          <div className="d-flex flex-column">
            <Link
              className="btn btn-light mt-4 justify-content-center align-items-center"
              to="/inputProfile"
              styles={{ textDecoration: "none" }}>
              <img src="./icons/fi_edit-3.svg" className="icon-btn" /> Ubah Akun
            </Link>
            <Link className="btn btn-light mt-4" to="/transaksi">
              <img src="./icons/fi_settings.svg" className="icon-btn" /> Riawayat Transaksi
            </Link>
            <button className="btn btn-light mt-4" onClick={() => logOut()}>
              <img src="./icons/fi_log-out.svg" className="icon-btn" /> Keluar
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProfilePage;
