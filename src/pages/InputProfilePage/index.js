import React, { useEffect, useState } from "react";
import "./styles.css";
import jwt_decode from "jwt-decode";

const InputProfilePage = () => {
  const [selectedFile, setSelectedFile] = useState();
  const [preview, setPreview] = useState();
  const [nama, setNama] = useState("");
  const [kota, setKota] = useState("");
  const [alamat, setAlamat] = useState("");
  const [nohp, setNohp] = useState("");
  const [showSuccessAlert, setShowSuccessAlert] = useState(false);
  const [showErrorAlert, setShowErrorAlert] = useState(false);
  const token = localStorage.getItem("token");

  useEffect(() => {
    if (!selectedFile) {
      setPreview(undefined);
      return;
    }

    const objectUrl = URL.createObjectURL(selectedFile);
    setPreview(objectUrl);

    return () => URL.revokeObjectURL(objectUrl);
  }, [selectedFile]);

  const onSelectFile = (e) => {
    if (!e.target.files || e.target.files.length === 0) {
      setSelectedFile(undefined);
      return;
    }

    setSelectedFile(e.target.files[0]);
  };

  const getUserProfile = jwt_decode(token);

  const url = "https://secondhand-api-fsw14-kel3.herokuapp.com/api/users/inputProfile";
  const SubmitInputProfile = async () => {
    console.log(selectedFile);
    const data = new FormData();
    data.append("foto", selectedFile);
    data.append("nama", nama);
    data.append("kota", kota);
    data.append("alamat", alamat);
    data.append("no_hp", nohp);

    let doSubmitProfile = await fetch(url, {
      method: "PUT",
      headers: {
        Authorization: "Bearer " + token
      },
      body: data
    })
      .then((res) => res.json())
      .then((data) => {
        return data;
      });
    if (doSubmitProfile.status === "success") {
      setShowSuccessAlert(true);
      window.location.href = "/";
    } else {
      setShowErrorAlert(true);
    }
  };

  return (
    <div className="container-fluid input-profile">
      <div className="row justify-content-center">
        <div className="col-lg-6 px-auto">
          <div className="mx-auto">
            <input type="file" onChange={onSelectFile} id="input-img" style={{ display: "none" }} />
            <label htmlFor="input-img" className="btn input-profile-img-btn mx-auto">
              {selectedFile ? (
                <img src={preview} className="input-profile-img" />
              ) : (
                <img
                  src="images/pilih-gambar.png"
                  alt="pilih gambar"
                  className="input-profile-img"
                />
              )}
            </label>
          </div>
          <div className="input-profile-field">
            <p className="label">Nama*</p>
            <input
              type="text"
              placeholder={getUserProfile == null ? "Nama" : getUserProfile.nama}
              className="input-profile-input"
              required
              autoComplete="off"
              value={nama}
              onChange={(e) => setNama(e.target.value)}
            />
          </div>
          <div className="input-profile-field">
            <p className="label">Kota*</p>
            <input
              type="text"
              placeholder="Surabaya"
              required
              className="input-profile-input"
              autoComplete="off"
              value={kota}
              onChange={(e) => setKota(e.target.value)}
            />
          </div>
          <div className="input-profile-field">
            <p className="label">Alamat*</p>
            <textarea
              type="text"
              placeholder="Contoh: Jalan Ikan Hiu 33"
              required
              autoComplete="off"
              value={alamat}
              onChange={(e) => setAlamat(e.target.value)}
            />
          </div>
          <div className="input-profile-field">
            <p className="label">No Handphone*</p>
            <input
              type="text"
              placeholder="contoh: +628123456789"
              required
              autoComplete="off"
              className="input-profile-input"
              value={nohp}
              onChange={(e) => setNohp(e.target.value)}
            />
          </div>
          <button type="button" className="btn input-profile-btn" onClick={SubmitInputProfile}>
            Simpan
          </button>
        </div>
      </div>
      {showSuccessAlert && (
        <div className="row justify-content-center">
          <div
            className="col-lg-6 alert detail-product-alert d-flex justify-content-between"
            role="alert">
            <p>Profile Berhasil Disimpan</p>
          </div>
        </div>
      )}
      {showErrorAlert && (
        <div className="row justify-content-center">
          <div
            className="col-lg-6 alert detail-product-alert d-flex justify-content-between"
            style={{ backgroundColor: "#bd0b0b" }}
            role="alert">
            <p>Coba Lagi</p>
            <button
              type="button"
              data-bs-dismiss="modal"
              onClick={() => setShowErrorAlert(false)}
              aria-label="Close">
              <img src="/icons/close-icon.svg" alt="close" />
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default InputProfilePage;
