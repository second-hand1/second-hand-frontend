import React, { useEffect, useState } from "react";
import "./Login.css";
import { Col, Row, Form, Button, InputGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import { AiOutlineEye, AiOutlineArrowLeft, AiFillEyeInvisible } from "react-icons/ai";
import SHD from "../../assets/images/SHD.png";

export default function LoginPage() {
  const [passwordShown, setPasswordShown] = useState(false);
  const togglePassword = () => {
    setPasswordShown(!passwordShown);
  };

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [showSuccessAlert, setShowSuccessAlert] = useState(false);
  const [showErrorAlert, setShowErrorAlert] = useState(false);
  const token = localStorage.getItem("token");

  useEffect(() => {
    setShowSuccessAlert(!!token);
  }, [token]);

  const url = "https://secondhand-api-fsw14-kel3.herokuapp.com/api/login";
  const login = async () => {
    const data = {
      email: email.toLowerCase(),
      password: password
    };
    var doLogin = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    })
      .then((response) => response.json())
      .then((data) => {
        return data;
      });
    if (doLogin.status === "success") {
      localStorage.setItem("token", doLogin.token);
      window.location.href = "/";
      setShowSuccessAlert(true);
    } else {
      setShowErrorAlert(true);
    }
  };

  return (
    <div>
      <Row>
        <Link to="/" className="arrow-back">
          <AiOutlineArrowLeft />
        </Link>
        <Col>
          <img src={SHD} className="img-fluid" alt="..." style={{ width: "100%" }}></img>
        </Col>
        <Col className="login-form">
          <Row className="row p-4 w-75 mx-auto align-items-center">
            <Col lg={12}>
              <h2>
                <b>Masuk</b>
              </h2>
              <Form>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Contoh: johndee@gmail.com"
                    style={{ borderRadius: "16px" }}
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Label>Password</Form.Label>
                  <InputGroup>
                    <Form.Control
                      type={passwordShown ? "text" : "password"}
                      placeholder="Masukkan password"
                      value={password}
                      onChange={(e) => setPassword(e.target.value)}
                      style={{
                        borderTopLeftRadius: "16px",
                        borderBottomLeftRadius: "16px"
                      }}></Form.Control>
                    <InputGroup.Text
                      onClick={togglePassword}
                      style={{ borderTopRightRadius: "16px", borderBottomRightRadius: "16px" }}>
                      {passwordShown ? <AiFillEyeInvisible /> : <AiOutlineEye />}
                    </InputGroup.Text>
                  </InputGroup>
                </Form.Group>

                <Button
                  // type="submit"
                  // value={"Login"}
                  variant="primary w-100"
                  onClick={login}
                  style={{ backgroundColor: "#7126B5", borderRadius: "16px" }}>
                  Masuk
                </Button>
              </Form>
              <p className="text-center custom-font">
                Belum punya akun?{" "}
                <Link to="/register" style={{ fontWeight: "bold", color: "#7126B5" }}>
                  Daftar di sini
                </Link>
              </p>
            </Col>
          </Row>
        </Col>
      </Row>
      {showSuccessAlert && (
        <div className="row justify-content-center">
          <div
            className="col-lg-6 alert detail-product-alert d-flex justify-content-between"
            role="alert">
            <p>Anda Berhasil Login</p>
          </div>
        </div>
      )}
      {showErrorAlert && (
        <div className="row justify-content-center">
          <div
            className="col-lg-6 alert detail-product-alert d-flex justify-content-between"
            style={{ backgroundColor: "#bd0b0b" }}
            role="alert">
            <p>Coba Masukan Email & password Yang Benar</p>
            <button
              type="button"
              data-bs-dismiss="modal"
              onClick={() => setShowErrorAlert(false)}
              aria-label="Close">
              <img src="/icons/close-icon.svg" alt="close" />
            </button>
          </div>
        </div>
      )}
    </div>
  );
}
