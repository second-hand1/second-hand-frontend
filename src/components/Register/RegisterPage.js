import React, { useState } from "react";
import "./Register.css";
import { Col, Row, Form, Button, InputGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import { AiOutlineEye, AiFillEyeInvisible, AiOutlineArrowLeft } from "react-icons/ai";
import SHD from "../../assets/images/SHD.png";

export default function RegisterPage() {
  const [passwordShown, setPasswordShown] = useState(false);
  const togglePassword = () => {
    setPasswordShown(!passwordShown);
  };

  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [showSuccessAlert, setShowSuccessAlert] = useState(false);
  const [showErrorAlert, setShowErrorAlert] = useState(false);

  const url = "https://secondhand-api-fsw14-kel3.herokuapp.com/api/users/register";
  const register = async () => {
    const data = {
      nama: username,
      email: email.toLowerCase(),
      password: password
    };
    let doRegister = await fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    })
      .then((res) => res.json())
      .then((data) => {
        return data;
      });
    if (doRegister.status === "success") {
      window.location.href = "/login";
      setShowSuccessAlert(true);
    } else {
      setShowErrorAlert(true);
    }
  };

  return (
    <div>
      <Row>
        <Link to="/" className="arrow-back">
          <AiOutlineArrowLeft />
        </Link>
        <Col className="hide-width-screen">
          <img src={SHD} className="img-fluid img_SHD" alt="..." style={{ width: "100%" }}></img>
        </Col>
        <Col className="Register-form">
          <Row className="row p-4 w-75 mx-auto align-items-center">
            <Col lg={12}>
              <h2>
                <b>Daftar</b>
              </h2>
              <Form>
                <Form.Group className="mb-3" controlId="formBasicName">
                  <Form.Label>Nama</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Nama"
                    style={{ borderRadius: "16px" }}
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Contoh: johndee@gmail.com"
                    style={{ borderRadius: "16px" }}
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Label>Password</Form.Label>
                  <InputGroup>
                    <Form.Control
                      type={passwordShown ? "text" : "password"}
                      placeholder="Masukkan password"
                      value={password}
                      onChange={(e) => setPassword(e.target.value)}
                      style={{
                        borderTopLeftRadius: "16px",
                        borderBottomLeftRadius: "16px"
                      }}></Form.Control>
                    <InputGroup.Text
                      onClick={togglePassword}
                      style={{ borderTopRightRadius: "16px", borderBottomRightRadius: "16px" }}>
                      {passwordShown ? <AiFillEyeInvisible /> : <AiOutlineEye />}
                    </InputGroup.Text>
                  </InputGroup>
                </Form.Group>

                <Button
                  variant="primary w-100"
                  onClick={password.length >= 8 && register}
                  style={{ backgroundColor: "#7126B5", borderRadius: "16px" }}>
                  Daftar
                </Button>
              </Form>
              <p className="text-center custom-font">
                Sudah punya akun?{" "}
                <Link to="/login" style={{ fontWeight: "bold", color: "#7126B5" }}>
                  Masuk di sini
                </Link>
              </p>
            </Col>
          </Row>
        </Col>
      </Row>
      {showSuccessAlert && (
        <div className="row justify-content-center">
          <div
            className="col-lg-6 alert detail-product-alert d-flex justify-content-between"
            role="alert">
            <p>Akun Berhasil Dibuat</p>
            <Link to="/login" style={{ color: "white", textDecoration: "none" }}>
              <b>Login Sekarang</b>
            </Link>
          </div>
        </div>
      )}
      {showErrorAlert && (
        <div className="row justify-content-center">
          <div
            className="col-lg-6 alert detail-product-alert d-flex justify-content-between"
            style={{ backgroundColor: "#bd0b0b" }}
            role="alert">
            <p>Akun Gagal Dibuat, Silahkan Coba Dengan Email Lain</p>
            <button
              type="button"
              data-bs-dismiss="modal"
              onClick={() => setShowErrorAlert(false)}
              aria-label="Close">
              <img src="/icons/close-icon.svg" alt="close" />
            </button>
          </div>
        </div>
      )}
    </div>
  );
}
