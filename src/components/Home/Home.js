import React, { useState, useEffect } from "react";
import "./Home.css";
import Carousel from "./Carousel/carousel";
import ProductCard from "../ProductCard";
import axios from "axios";
import { Row, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import Buttonjual from "../../assets/images/jualButton.png";
import { AiOutlineSearch } from "react-icons/ai";
import EmptyList from "../EmptyList";

export default function Home() {
  const url = "https://secondhand-api-fsw14-kel3.herokuapp.com/api/barang/list";
  const urlHobi = "https://secondhand-api-fsw14-kel3.herokuapp.com/api/barang/list/hobi";
  const urlKendaraan = "https://secondhand-api-fsw14-kel3.herokuapp.com/api/barang/list/kendaraan";
  const urlBaju = "https://secondhand-api-fsw14-kel3.herokuapp.com/api/barang/list/baju";
  const urlElektronik =
    "https://secondhand-api-fsw14-kel3.herokuapp.com/api/barang/list/elektronik";
  const urlKesehatan = "https://secondhand-api-fsw14-kel3.herokuapp.com/api/barang/list/kesehatan";
  const bearerToken = localStorage.getItem("token");
  const isLogin = bearerToken !== null;

  const [dataProduct, setDataProduct] = useState([]);
  const [dataProductHobi, setDataProductHobi] = useState([]);
  const [dataProductKendaraan, setDataProductKendaraan] = useState([]);
  const [dataProductBaju, setDataProductBaju] = useState([]);
  const [dataProductElektronik, setDataProductElektronik] = useState([]);
  const [dataProductKesehatan, setDataProductKesehatan] = useState([]);

  const getPost = async () => {
    await axios.get(url).then((res) => {
      setDataProduct(res.data);
    });
  };

  const getPostHobi = async () => {
    await axios.get(urlHobi).then((res) => {
      setDataProductHobi(res.data);
    });
  };

  const getPostKendaraan = async () => {
    await axios.get(urlKendaraan).then((res) => {
      setDataProductKendaraan(res.data);
    });
  };

  const getPostBaju = async () => {
    await axios.get(urlBaju).then((res) => {
      setDataProductBaju(res.data);
    });
  };

  const getPostElektronik = async () => {
    await axios.get(urlElektronik).then((res) => {
      setDataProductElektronik(res.data);
    });
  };

  const getPostKesehatan = async () => {
    await axios.get(urlKesehatan).then((res) => {
      setDataProductKesehatan(res.data);
    });
  };

  useEffect(() => {
    getPost();
    getPostHobi();
    getPostKendaraan();
    getPostBaju();
    getPostElektronik();
    getPostKesehatan();
  }, []);

  const dataProducts = dataProduct.data;
  const dataProductsHobi = dataProductHobi.data;
  const dataProductsKendaraan = dataProductKendaraan.data;
  const dataProductsBaju = dataProductBaju.data;
  const dataProductsElektronik = dataProductElektronik.data;
  const dataProductsKesehatan = dataProductKesehatan.data;

  return (
    <div style={{ marginTop: "100px" }}>
      <Carousel />
      <Container>
        <Row>
          <div className="home-kategori">
            <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
              <li className="nav-item" role="presentation">
                <button
                  className="nav-link active"
                  id="v-pills-semua-tab"
                  data-bs-toggle="pill"
                  data-bs-target="#v-pills-semua"
                  type="button"
                  role="tab"
                  aria-controls="v-pills-semua"
                  aria-selected="true">
                  <div className="d-flex justify-content-between ">
                    <AiOutlineSearch />
                    <p className="flex-fill">Semua</p>
                  </div>
                </button>
              </li>
              <li className="nav-item" role="presentation">
                <button
                  className="nav-link"
                  id="v-pills-hobi-tab"
                  data-bs-toggle="pill"
                  data-bs-target="#v-pills-hobi"
                  type="button"
                  role="tab"
                  aria-controls="v-pills-hobi"
                  aria-selected="false">
                  <div className="d-flex justify-content-between ">
                    <AiOutlineSearch />
                    <p className="flex-fill">Hobi</p>
                  </div>
                </button>
              </li>
              <li className="nav-item" role="presentation">
                <button
                  className="nav-link"
                  id="v-pills-kendaraan-tab"
                  data-bs-toggle="pill"
                  data-bs-target="#v-pills-kendaraan"
                  type="button"
                  role="tab"
                  aria-controls="v-pills-kendaraan"
                  aria-selected="false">
                  <div className="d-flex justify-content-between ">
                    <AiOutlineSearch />
                    <p className="flex-fill">Kendaraan</p>
                  </div>
                </button>
              </li>
              <li className="nav-item" role="presentation">
                <button
                  className="nav-link"
                  id="v-pills-baju-tab"
                  data-bs-toggle="pill"
                  data-bs-target="#v-pills-baju"
                  type="button"
                  role="tab"
                  aria-controls="v-pills-baju"
                  aria-selected="false">
                  <div className="d-flex justify-content-between ">
                    <AiOutlineSearch />
                    <p className="flex-fill">Baju</p>
                  </div>
                </button>
              </li>
              <li className="nav-item" role="presentation">
                <button
                  className="nav-link"
                  id="v-pills-elektronik-tab"
                  data-bs-toggle="pill"
                  data-bs-target="#v-pills-elektronik"
                  type="button"
                  role="tab"
                  aria-controls="v-pills-elektronik"
                  aria-selected="false">
                  <div className="d-flex justify-content-between ">
                    <AiOutlineSearch />
                    <p className="flex-fill">Elektronik</p>
                  </div>
                </button>
              </li>
              <li className="nav-item" role="presentation">
                <button
                  className="nav-link"
                  id="v-pills-kesehatan-tab"
                  data-bs-toggle="pill"
                  data-bs-target="#v-pills-kesehatan"
                  type="button"
                  role="tab"
                  aria-controls="v-pills-kesehatan"
                  aria-selected="false">
                  <div className="d-flex justify-content-between ">
                    <AiOutlineSearch />
                    <p className="flex-fill">Kesehatan</p>
                  </div>
                </button>
              </li>
            </ul>
          </div>
        </Row>
      </Container>
      <Container className="list-barang-home">
        <Row>
          <div className="tab-content" id="v-pills-tabContent">
            <div
              className="tab-pane fade show active"
              id="v-pills-semua"
              role="tabpanel"
              aria-labelledby="v-pills-semua-tab">
              <div className="col-lg-12 mx-auto row">
                {dataProducts == undefined ? (
                  <img
                    href="./images/undraw_aircraft_re_m05i.svg"
                    className="loading-image mx-auto"
                  />
                ) : (
                  dataProducts.map((item) => {
                    const image = JSON.parse(item.foto_produk);
                    return (
                      <div key={item.id} className="col-lg-2 col-md-5 col-sm-5 mb-4">
                        <ProductCard
                          img={image[0]}
                          name={item.nama_produk}
                          category={item.kategori}
                          price={item.harga_produk}
                          href={isLogin == true ? `/detailProduct?id=${item.id}` : `/login`}
                        />
                      </div>
                    );
                  })
                )}
              </div>
            </div>
            <div
              className="tab-pane fade"
              id="v-pills-hobi"
              role="tabpanel"
              aria-labelledby="v-pills-hobi-tab">
              <div className="col-lg-12 mx-auto row">
                {dataProductsHobi == undefined ? (
                  <img
                    href="./images/undraw_aircraft_re_m05i.svg"
                    className="loading-image mx-auto"
                  />
                ) : dataProductsHobi.length === 0 ? (
                  <EmptyList type="home" />
                ) : (
                  dataProductsHobi.map((item) => {
                    const image = JSON.parse(item.foto_produk);
                    return (
                      <div key={item.id} className="col-lg-2 col-md-5 col-sm-5 mb-4">
                        <ProductCard
                          img={image[0]}
                          name={item.nama_produk}
                          category={item.kategori}
                          price={item.harga_produk}
                          href={isLogin == true ? `/detailProduct?id=${item.id}` : `/login`}
                        />
                      </div>
                    );
                  })
                )}
              </div>
            </div>
            <div
              className="tab-pane fade"
              id="v-pills-kendaraan"
              role="tabpanel"
              aria-labelledby="v-pills-kendaraan-tab">
              <div className="col-lg-12 mx-auto row">
                {dataProductsKendaraan == undefined ? (
                  <img
                    href="./images/undraw_aircraft_re_m05i.svg"
                    className="loading-image mx-auto"
                  />
                ) : dataProductsKendaraan.length === 0 ? (
                  <EmptyList type="home" />
                ) : (
                  dataProductsKendaraan.map((item) => {
                    const image = JSON.parse(item.foto_produk);
                    return (
                      <div key={item.id} className="col-lg-2 col-md-5 col-sm-5 mb-4">
                        <ProductCard
                          img={image[0]}
                          name={item.nama_produk}
                          category={item.kategori}
                          price={item.harga_produk}
                          href={isLogin == true ? `/detailProduct?id=${item.id}` : `/login`}
                        />
                      </div>
                    );
                  })
                )}
              </div>
            </div>
            <div
              className="tab-pane fade"
              id="v-pills-baju"
              role="tabpanel"
              aria-labelledby="v-pills-baju-tab">
              <div className="col-lg-12 mx-auto row">
                {dataProductsBaju == undefined ? (
                  <img
                    href="./images/undraw_aircraft_re_m05i.svg"
                    className="loading-image mx-auto"
                  />
                ) : dataProductsBaju.length === 0 ? (
                  <EmptyList type="home" />
                ) : (
                  dataProductsBaju.map((item) => {
                    const image = JSON.parse(item.foto_produk);
                    return (
                      <div key={item.id} className="col-lg-2 col-md-5 col-sm-5 mb-4">
                        <ProductCard
                          img={image[0]}
                          name={item.nama_produk}
                          category={item.kategori}
                          price={item.harga_produk}
                          href={isLogin == true ? `/detailProduct?id=${item.id}` : `/login`}
                        />
                      </div>
                    );
                  })
                )}
              </div>
            </div>
            <div
              className="tab-pane fade"
              id="v-pills-elektronik"
              role="tabpanel"
              aria-labelledby="v-pills-elektronik-tab">
              <div className="col-lg-12 mx-auto row">
                {dataProductsElektronik == undefined ? (
                  <img
                    href="./images/undraw_aircraft_re_m05i.svg"
                    className="loading-image mx-auto"
                  />
                ) : dataProductsElektronik.length === 0 ? (
                  <EmptyList type="home" />
                ) : (
                  dataProductsElektronik.map((item) => {
                    const image = JSON.parse(item.foto_produk);
                    return (
                      <div key={item.id} className="col-lg-2 col-md-5 col-sm-5 mb-4">
                        <ProductCard
                          img={image[0]}
                          name={item.nama_produk}
                          category={item.kategori}
                          price={item.harga_produk}
                          href={isLogin == true ? `/detailProduct?id=${item.id}` : `/login`}
                        />
                      </div>
                    );
                  })
                )}
              </div>
            </div>
            <div
              className="tab-pane fade"
              id="v-pills-kesehatan"
              role="tabpanel"
              aria-labelledby="v-pills-kesehatan-tab">
              <div className="col-lg-12 mx-auto row">
                {dataProductsKesehatan == undefined ? (
                  <img
                    href="./images/undraw_aircraft_re_m05i.svg"
                    className="loading-image mx-auto"
                  />
                ) : dataProductsKesehatan.length === 0 ? (
                  <EmptyList type="home" />
                ) : (
                  dataProductsKesehatan.map((item) => {
                    const image = JSON.parse(item.foto_produk);
                    return (
                      <div key={item.id} className="col-lg-2 col-md-5 col-sm-5 mb-4">
                        <ProductCard
                          img={image[0]}
                          name={item.nama_produk}
                          category={item.kategori}
                          price={item.harga_produk}
                          href={isLogin == true ? `/detailProduct?id=${item.id}` : `/login`}
                        />
                      </div>
                    );
                  })
                )}
              </div>
            </div>
          </div>
        </Row>
        {isLogin === true && (
          <Link to="/inputProduk">
            <img src={Buttonjual} className="imgJual" alt="..."></img>
          </Link>
        )}
      </Container>
    </div>
  );
}
