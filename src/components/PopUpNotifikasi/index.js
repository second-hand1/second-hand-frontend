import React, { useState } from "react";
import "./styles.css";
import ProductCardLebar from "../ProductCardLebar";
import axios from "axios";
import { useEffect } from "react";

const PopUpNotifikasi = () => {
  const [alert, setAlert] = useState(true);
  const bearerToken = localStorage.getItem("token");

  setTimeout(() => {
    setAlert(false);
  }, 10000);

  const urlNotifikasi = "https://secondhand-api-fsw14-kel3.herokuapp.com/api/tawaran/barangku";

  const [notifikasi, setNotifikasi] = useState([]);

  const getNotifikasi = async () => {
    await axios
      .get(urlNotifikasi, {
        headers: {
          Authorization: `Bearer ${bearerToken}`
        }
      })
      .then((res) => {
        setNotifikasi(res.data);
      });
  };

  useEffect(() => {
    getNotifikasi();
  }, []);

  const dataNotifikasi = notifikasi && notifikasi.data ? notifikasi.data : [];

  return (
    <div>
      {alert && (
        <div className="row justify-content-end">
          <div
            className="col-lg-6 alert popup-notifikasi "
            role="alert"
            style={{ marginTop: "50px" }}>
            <div className="popup-notifikasi-head d-flex justify-content-between align-items-center px-3">
              <p>Notifikasi Terbaru</p>
              <button
                type="button"
                data-bs-dismiss="modal"
                onClick={() => setAlert(false)}
                aria-label="Close">
                <img src="/icons/close-icon-black.svg" alt="close" />
              </button>
            </div>
            <div className="px-3">
              {dataNotifikasi == []
                ? ""
                : dataNotifikasi
                    .sort((a, b) => b.updateAt - a.updateAt)
                    .slice(0, 2)
                    .map((item) => {
                      const image = JSON.parse(item.Barang.foto_produk);
                      return (
                        <ProductCardLebar
                          key={item.id}
                          harga={item.Barang.harga_produk}
                          nama={item.User.nama}
                          hargaPenawaran={item.harga_tawaran == null ? 0 : item.harga_tawaran}
                          tanggalPenawaran={item.updateAt}
                          type="Penawaran"
                          href={`/detailPenawaran?id=${item.id}`}
                          image={image[0]}
                        />
                      );
                    })}
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default PopUpNotifikasi;
