/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import "./Input.css";
import { Container, Form, Button, Row, Col, InputGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import CurrencyInput from "react-currency-input-field";
import { AiOutlineArrowLeft } from "react-icons/ai";
import fotoProduk from "../assets/images/fotoProduk.png";
import jwt_decode from "jwt-decode";

export default function InputPage() {
  const token = localStorage.getItem("token");
  const isLogin = token !== null;

  const getUserData = jwt_decode(token);

  const dateNow = new Date();
  const dateNowConvert = dateNow.getTime().toString().slice(0, 10);

  if (isLogin === false || getUserData.exp < dateNowConvert) {
    window.location.href = "/login";
  }

  const [selectedImages, setSelectedImages] = useState([]);
  const [selectedImagesTwo, setSelectedImagesTwo] = useState([]);
  const data = new FormData();
  const onSelectFile = (e) => {
    const selectedFiles = e.target.files;
    const selectedFilesArray = Array.from(selectedFiles);

    setSelectedImagesTwo([...selectedImagesTwo, selectedFiles]);
    const imagesArray = selectedFilesArray.map((file) => {
      return URL.createObjectURL(file);
    });

    setSelectedImages((previousImages) => previousImages.concat(imagesArray));
  };

  const [nama, setNama] = useState("");
  const [harga, setHarga] = useState("");
  const [kategori, setKategori] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [showSuccessAlert, setShowSuccessAlert] = useState(false);
  const [showErrorAlert, setShowErrorAlert] = useState(false);

  const url = "https://secondhand-api-fsw14-kel3.herokuapp.com/api/barang/create";
  const submitInputProduct = async () => {
    data.append("nama_produk", nama);
    data.append("harga_produk", harga);
    data.append("kategori", kategori);
    data.append("deskripsi", deskripsi);
    const images = [];
    selectedImagesTwo.forEach((image) => {
      data.append("foto", image[0], image[0].name);
    });

    let doSubmitProduct = await fetch(url, {
      method: "POST",
      headers: {
        // "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${token}`
      },
      body: data
    })
      .then((res) => res.json())
      .then((data) => {
        return data;
      });
    if (doSubmitProduct.status === "success") {
      setShowSuccessAlert(true);
      window.location.href = "/";
    } else {
      setShowErrorAlert(true);
    }
  };

  return (
    <Container>
      <Row>
        <Col lg={1}>
          <Link to="/" className="arrow-back mt-5">
            <AiOutlineArrowLeft />
          </Link>
        </Col>
        <Col lg={11}>
          <p className="InfoProduk">
            <b>Lengkapi Detail Produk</b>
          </p>
        </Col>
      </Row>
      <Form>
        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
          <Form.Label>Nama Produk</Form.Label>
          <Form.Control
            type="text"
            placeholder="nama produk"
            className="customForm"
            value={nama}
            onChange={(e) => setNama(e.target.value)}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="exampleForm.ControlInput2">
          <Form.Label>Harga Produk</Form.Label>
          <InputGroup>
            <CurrencyInput
              className="InputHarga"
              id="input-example"
              name="input-name"
              placeholder="Rp 0.00"
              decimalsLimit={3}
              prefix={"Rp"}
              onChange={(e) => setHarga(e.target.value)}
            />
          </InputGroup>
        </Form.Group>
        <Form.Group className="mb-3" controlId="exampleForm.ControlInput3">
          <Form.Label>Katagori</Form.Label>
          <Form.Select
            aria-label="Default select example"
            className="customForm"
            onChange={(e) => setKategori(e.target.value)}>
            <option>Pilih Katagori</option>
            <option value="hobi">Hobi</option>
            <option value="kendaraan">Kendaraan</option>
            <option value="baju">Baju</option>
            <option value="elektronik">Elektronik</option>
            <option value="kesehatan">Kesehatan</option>
          </Form.Select>
        </Form.Group>
        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
          <Form.Label>Deskripsi</Form.Label>
          <Form.Control
            as="textarea"
            rows={3}
            placeholder="Contoh: Jalan Ikan Hiu 33"
            className="customForm"
            value={deskripsi}
            onChange={(e) => setDeskripsi(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="formFile" className="mb-3">
          <Form.Label>Foto Produk</Form.Label>
          <InputGroup>
            <label className="labelFoto">
              <img src={fotoProduk} />
              <input type="file" className="customInput" onChange={onSelectFile} multiple />
            </label>
            <br />
            <div className="images">
              {selectedImages &&
                selectedImages.map((image) => {
                  return (
                    <div key={image} className="image">
                      <img src={image} height="150" alt="upload" />
                      <br />
                      <Button
                        className="delete"
                        onClick={() =>
                          setSelectedImages(selectedImages.filter((e) => e !== image))
                        }>
                        Delete
                      </Button>
                    </div>
                  );
                })}
            </div>
          </InputGroup>
        </Form.Group>
        <Row>
          <Col lg={6}>
            <Button variant=" w-100 customButton1" type="submit">
              Preview
            </Button>
          </Col>
          <Col lg={6}>
            {selectedImages.length > 0 &&
              (selectedImages.length > 5 ? (
                <p>You cant upload more than 5 images!</p>
              ) : (
                <Button variant=" w-100 customButton2" onClick={submitInputProduct}>
                  Terbitkan
                </Button>
              ))}
          </Col>
        </Row>
      </Form>
      {showSuccessAlert && (
        <div className="row justify-content-center">
          <div
            className="col-lg-6 alert detail-product-alert d-flex justify-content-between"
            role="alert">
            <p>Barang Berhasil Disimpan</p>
          </div>
        </div>
      )}
      {showErrorAlert && (
        <div className="row justify-content-center">
          <div
            className="col-lg-6 alert detail-product-alert d-flex justify-content-between"
            style={{ backgroundColor: "#bd0b0b" }}
            role="alert">
            <p>Coba Lagi</p>
            <button
              type="button"
              data-bs-dismiss="modal"
              onClick={() => setShowErrorAlert(false)}
              aria-label="Close">
              <img src="/icons/close-icon.svg" alt="close" />
            </button>
          </div>
        </div>
      )}
    </Container>
  );
}
