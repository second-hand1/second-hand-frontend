import React from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import "./Input.css";

export default function NavbarInput() {
  return (
    <Navbar id="Navbar" className="navBg" expand="lg">
      <Container>
        <Navbar.Brand href="/" className="navLogo"></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/"></Nav.Link>
            <Nav.Link href="/"></Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
