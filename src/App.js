/* eslint-disable prettier/prettier */
import React from "react";
import HomePage from "./pages/HomePage/HomePage";
import Login from "./pages/Login";
import Register from "./pages/Register";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import InputProfilePage from "./pages/InputProfilePage";
import ProfilePage from "./pages/ProfilePage";
import DetailProductPages from "./pages/DetailProductPages/DetailProduct";
import DetailProductSellerPages from "./pages/DetailProductSellerPages/DetailProductSeller";
import DaftarJualPage from "./pages/DaftarJualPage/DaftarJual";
import InputPage from "./pages/InputPage";
import NotifikasiPage from "./pages/NotifikasiPage";
import DetailPenawaranPage from "./pages/DetailPenawaranPage";
import UpdatePage from "./pages/UpdateProduct";
import WishlistPage from "./pages/WishlistPage";
import TransaksiPage from "./pages/TransaksiPage";

export default function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/" element={<HomePage />} />
        <Route path="/inputProfile" element={<InputProfilePage />} />
        <Route path="/profile" element={<ProfilePage />} />
        <Route path="/detailProduct" element={<DetailProductPages />} />
        <Route path="/sellerProduct" element={<DetailProductSellerPages />} />
        <Route path="/daftarJual" element={<DaftarJualPage />} />
        <Route path="/inputProduk" element={<InputPage />} />
        <Route path="/updateProduk" element={<UpdatePage />} />
        <Route path="/notifikasi" element={<NotifikasiPage />} />
        <Route path="/transaksi" element={<TransaksiPage />} />
        <Route path="/detaiPenawaran" element={<DetailPenawaranPage />} />
        <Route path="/wishlist" element={<WishlistPage />} />
      </Routes>
    </BrowserRouter>
  );
}
